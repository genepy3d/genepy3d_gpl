# -*- coding: utf-8 -*-

import numpy as np
from genepy3d.obj import surfaces, curves
from genepy3d_gpl.interact import curvesurface

def test_intersect_curve_surface():
    
    # create test surface
    triange_coors = np.array([[0.,0.,0.],[4.,0.,0.],[2.,2.,0.],[4.,9.,0.]])
    surf = surfaces.Surface(triange_coors,np.array([[0,1,2],[1,2,3]]))
    
    seg_coors = np.array([[0.,4.,0.],[5.,4.,0.],[5.,4.,3.],[2.,1.,0.]])
    crv = curves.Curve(seg_coors)
    
    pnts, crvids = curvesurface.intersect(crv,surf)
    assert len(pnts.coors)==3.
    assert pnts.coors[2,0]==2.
    assert pnts.coors[2,1]==1.
    assert crvids[0]==0
    assert crvids[1]==0
    assert crvids[2]==2
    
def test_inonout_curve_surface():
    
    # create test surface
    triange_coors = np.array([[1.,0.,0.],[0.,1.,0.],[0.,0.,1.],[0.,0.,0.]])
    surf = surfaces.Surface(triange_coors,np.array([[0,1,2],[0,1,3],[0,2,3]]))
    
    pnt_coors = np.array([[0.25,0.25,0.1],[0.25,0.25,0.25],[0.,1.,0.],[0.,0.,1.],[0.5,0.5,0.5],[1.,0.5,1.]])
    crv = curves.Curve(pnt_coors)
    
    incrvs, oncrvs, outcrvs = curvesurface.inonout(crv,surf)
    assert len(incrvs[0].coors)==3
    assert len(oncrvs[0].coors)==2
    assert len(outcrvs[0].coors)==3
    

# -*- coding: utf-8 -*-
import numpy as np
from genepy3d.obj import points, surfaces
from genepy3d_gpl.interact import pointsurface

def test_inside():
    # create test surface
    triange_coors = np.array([[1.,0.,0.],[0.,1.,0.],[0.,0.,1.],[0.,0.,0.]])
    surf = surfaces.Surface(triange_coors,np.array([[0,1,2],[0,1,3],[0,2,3]]))
    pnt_coors = np.array([[0.25,0.25,0.1],[0.25,0.25,0.25],[0.,1.,0.],[0.5,0.5,0.5],[1.,0.5,1.]])
    pnts = points.Points(pnt_coors)
    
    inside, onside, outside = pointsurface.inonout(pnts,surf,return_masks=True)

    print("\n")
    print("Inside flag:",inside)
    print("Onside flag:",onside)
    print("Outside flag:",outside)

    assert inside[0]==True
    assert onside[2]==True
    assert outside[3]==True
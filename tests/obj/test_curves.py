# -*- coding: utf-8 -*-

import numpy as np

from genepy3d.obj import curves
from genepy3d_gpl.obj import curves as curves_gpl

class TestCurve:
    """Basic test of Curve class
    """
    
    def test_intersection(self):
        
        coors1 = np.array([[0.,1.,0.],[1.,0.,0.],[2.,0.,0.],[3.,3.,0.],[5.,3.,0.]])
        curve1 = curves.Curve(coors1)
    
        coors2 = np.array([[0.,0.5,0.],[3.,0.5,0.],[4.,4.,0.]])
        curve2 = curves.Curve(coors2)
        
        intersections = curves_gpl.intersect(curve1, curve2)
        assert intersections.coors[0,0]==0.5
        assert intersections.coors[1,1]==0.5
        assert intersections.coors[2,1]==3.
        
    
    

    
    

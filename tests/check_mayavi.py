"""Test if Mayavi is working.
"""

import numpy as np
from mayavi import mlab
from genepy3d.obj.curves import Curve
from genepy3d_gpl.util import mayavi as mvi

print("\nStart testing Mayavi... If you see the plot, so Mayavi is working. Then, please close the plot.")

# Helix
t = np.arange(50)
a = 1.
b = 1.
x = a * np.cos(t/5)
y = a * np.sin(t/5)
z = b * t
crv = Curve((x,y,z))

r = np.abs(np.sin(t)*0.2) + 0.1 # random radius

mlab.figure(1, bgcolor=(0, 0, 0), fgcolor=(1, 1, 1), size=(800, 600))
mlab.clf()
mvi.plot_curve(mlab,crv,r=r,clc=(0.,1.,0.))
mlab.show()

print("Test Mayavi DONE.")
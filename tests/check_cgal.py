"""Test if CGAL is working.
"""

import numpy as np
import genepy3d.obj.points as points
from genepy3d_gpl.obj.points import process
import matplotlib.pyplot as plt

print("\nStart testing CGAL... If you see the plot, so CGAL is working. Then, please close the plot.")

# Generate an ellipsoid
P = points.gen_ellipsoid(axes_length=[1.,0.7,0.5], n=300)

# Add noise
new_coors = P.coors + np.random.normal(scale=0.1,size=P.coors.shape)
PNoise = points.Points(new_coors)

# Process the points cloud
PProcessed = process(PNoise,removed_percentage=5,smooth=True)

fig = plt.figure()
ax = fig.add_subplot(111,projection='3d')
PNoise.plot(ax,point_args={"alpha":0.3,"color":"green"})
PProcessed.plot(ax,point_args={"alpha":0.3,"color":"magenta"})
ax.axis('off');
plt.tight_layout();
plt.show()

print("Test CGAL DONE.")

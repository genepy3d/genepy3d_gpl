# genepy3d-gpl

Genepy3d functions using packages under GPL-like license.

Further details can be found on https://genepy3d.gitlab.io/.

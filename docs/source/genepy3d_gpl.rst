genepy3d\_gpl package
=====================

Module contents
---------------

.. automodule:: genepy3d_gpl
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   genepy3d_gpl.interact
   genepy3d_gpl.obj
   genepy3d_gpl.util


genepy3d\_gpl.interact package
==============================

Module contents
---------------

.. automodule:: genepy3d_gpl.interact
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:

Submodules
----------

genepy3d\_gpl.interact.curvesurface module
------------------------------------------

.. automodule:: genepy3d_gpl.interact.curvesurface
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:

genepy3d\_gpl.interact.pointsurface module
------------------------------------------

.. automodule:: genepy3d_gpl.interact.pointsurface
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:


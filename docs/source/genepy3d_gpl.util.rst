genepy3d\_gpl.util package
==========================

Module contents
---------------

.. automodule:: genepy3d_gpl.util
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:

Submodules
----------

genepy3d\_gpl.util.mayavi module
--------------------------------

.. automodule:: genepy3d_gpl.util.mayavi
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:


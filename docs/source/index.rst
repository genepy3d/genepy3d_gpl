.. genepy3d documentation master file, created by
   sphinx-quickstart on Wed Sep 18 15:44:48 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GeNePy3D_GPL documentation
==========================

You find here the reference documentation of GeNePy3D_GPL, a python library for quantitative geometry.
GeNePy3D_GPL consists of functions that link to the packages released under the GPL license. 
It is different to `GeNePy3D <https://gitlab.com/genepy3d/genepy3d>`_ which is released under BSD 3-Clause license.

For the installation, tutorial and application, please check its main page: https://genepy3d.gitlab.io.

The GeNePy3D_GPL source code is found here: https://gitlab.com/genepy3d/genepy3d_gpl/.

.. toctree::
   :maxdepth: 4

   genepy3d_gpl

Funding
=======

This development was originaly supported by Agence Nationale de la Recherche (contract ANR-11-EQPX-0029 Morphoscope2).

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

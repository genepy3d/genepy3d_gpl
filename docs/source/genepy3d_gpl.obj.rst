genepy3d\_gpl.obj package
=========================

Module contents
---------------

.. automodule:: genepy3d_gpl.obj
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:

Submodules
----------

genepy3d\_gpl.obj.curves module
-------------------------------

.. automodule:: genepy3d_gpl.obj.curves
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:

genepy3d\_gpl.obj.points module
-------------------------------

.. automodule:: genepy3d_gpl.obj.points
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:

genepy3d\_gpl.obj.surfaces module
---------------------------------

.. automodule:: genepy3d_gpl.obj.surfaces
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:



